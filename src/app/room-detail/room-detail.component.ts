import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormControl } from '@angular/forms';
import { ThisReceiver } from '@angular/compiler';
@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html',
  styleUrls: ['./room-detail.component.css']
})
export class RoomDetailComponent implements OnInit {
  showOverlay:any = false;
  booked:any = false;
  id:any;
  roomDetails:any;
  userDetails:any;
  fromDate :any= new FormControl('');
  toDate :any = new FormControl(''); 
  bookedDates:any =[];
  showFromAlert:any = false ;
  showToAlert:any = false ;
  bookedAlert:any = false ;

  wrongDateSelection:any = false;
  constructor(private location:Location,private route: ActivatedRoute,private router:Router,private db:AngularFirestore) { }

  ngOnInit(): void {
    this.bookedDates=[];
    this.showFromAlert = false;
    this.showToAlert = false;
    this.wrongDateSelection = false;
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    this.getObjectById(this.id).subscribe( data => {
      console.log(data);
      this.roomDetails =  data;
  })

  this.getBookingsDate().subscribe( (data: any) => {
    console.log(data);
    for(let i=0;i<data.length;i++)
    {
       let dates ={
        fromDate:data[i].fromDate,
        toDate:data[i].toDate
       }
       this.bookedDates.push(dates);
    }
   console.log(this.bookedDates); 
   });


  this.userDetails = JSON.parse(localStorage.getItem('user')!);
  console.log(this.userDetails.email);
  
    this.showOverlay = false;
    this.booked = false;
  }

  showPopup()
  {
    this.showOverlay = true;
  }

  closeOverlay()
  {
    this.showOverlay = false; 
  }

  bookRoom()
  {
    console.log(this.fromDate.value);
    console.log(this.toDate.value);
    if((this.fromDate.value > this.toDate.value)||this.fromDate.value.length == 0 || this.toDate.value.length == 0)
    {
      console.log("choose date properly");  
      this.wrongDateSelection = true;
    }
    else
    {
      this.wrongDateSelection = false;
    }

    for(let i=0;i<this.bookedDates.length;i++)
    {
      // console.log(this.bookedDates[i].fromDate +"<="+  this.fromDate.value +"&&"+this.bookedDates[i].toDate +" <= "+ this.toDate.value);
      
      if( (this.bookedDates[i].fromDate > this.fromDate.value && this.bookedDates[i].fromDate < this.toDate.value)
        ||
        (this.bookedDates[i].toDate > this.fromDate.value && this.bookedDates[i].toDate < this.toDate.value)
        )
       {
        this.bookedAlert = true ;
         console.log("true");
         
         break
       } 
       else
       {
        this.bookedAlert = false ;
        console.log("false");
       }
    }
    let post_data = {
      room_id:this.id,
      room_no:this.roomDetails.roomNo,
      user_email:this.userDetails.email,
      fromDate:this.fromDate.value,
      toDate:this.toDate.value,
      status:'pending'
    }

    if(this.wrongDateSelection == false && this.showFromAlert == false && this.showToAlert == false && this.bookedAlert == false  )
    {
      console.log("bookeddd !!!!!!!!!!!!!!!!!");
      
      this.db.collection('bookings').add(post_data)
      .then((res)=>{
        console.log(res);
        this.booked = true;
      })
    }
  
   

  }

  getBookingsDate() {
    let data = this.db.collection('bookings', ref => ref.where('room_id','==',this.id  )).valueChanges()
    return data;
  };

  goBack()
  {
    this.location.back();
  }

  getObjectById(id:any) { 
    return this.db.collection('rooms').doc(id).valueChanges()
   }

   onChangeFromDate()
   {
    this.wrongDateSelection = false;
    this.bookedAlert = false ;
    // console.log(date);
    for(let i=0;i<this.bookedDates.length;i++)
    {
      if(this.fromDate.value >= this.bookedDates[i].fromDate && this.fromDate.value <= this.bookedDates[i].toDate )                             
      {
        console.log("already booked");
        this.showFromAlert = true;
        break;
      }
      else
      {
        this.showFromAlert = false;
        console.log("new booking");
      }
    }

   
   }


   onChangeToDate()
   {
    this.wrongDateSelection = false;
    this.bookedAlert = false ;
    for(let i=0;i<this.bookedDates.length;i++)
    {
      if(this.toDate.value >= this.bookedDates[i].fromDate && this.toDate.value <= this.bookedDates[i].toDate  )
      {
        console.log("already booked");
        this.showToAlert = true;
        break;
      }
      else
      {
        this.showToAlert = false;
        console.log("new booking");
      }
    }
   }

}
